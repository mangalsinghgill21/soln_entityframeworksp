﻿using entityframework.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace entityframework.common_repository
{
   public interface Idelete
    {
       public async Task<Boolean> DeleteAsync(user userentityobj);
    }
}
