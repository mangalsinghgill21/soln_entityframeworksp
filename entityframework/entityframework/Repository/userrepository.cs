﻿using entityframework.Entity;
using entityframework.ER;
using entityframework.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace entityframework.Repository
{
    class userrepository : Iuserrepository
    {
        #region Declarattion
        private testdbEntities db = null;
        #endregion

        #region Constructor
        public userrepository()
        {
            db = new testdbEntities();
        }
        #endregion

        #region public methods
        
        #region insert

        public async Task<Boolean> InsertAsync(user userentityobj)
        {
           int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {

                    var setQuery =
                     db.uspuserset
                     (
                         "Insert",
                         userentityobj.userid,
                         userentityobj.firstname,
                         userentityobj.lastname,
                         userentityobj.usercommunication.mobileno,
                         userentityobj.usercommunication.emailid,
                         userentityobj.userlogin.username,
                         userentityobj.userlogin.password);
                   
                     return (Convert.ToInt32(status.Value) == 1) ? true : false;   
                });
                    
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region update
        public async Task<Boolean> UpdateAsync(user userentityobj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {

                    var setQuery =
                     db.uspuserset
                     (
                         "Update",
                         userentityobj.userid,
                         userentityobj.firstname,
                         userentityobj.lastname,
                         userentityobj.usercommunication.mobileno,
                         userentityobj.usercommunication.emailid,
                         userentityobj.userlogin.username,
                         userentityobj.userlogin.password);

                    return (Convert.ToInt32(status.Value) == 1) ? true : false;
                });

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Delete
        public async Task<Boolean> DeleteAsync(user userentityobj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {

                    var setQuery =
                     db.uspuserset
                     (
                         "Delete",
                         userentityobj.userid,
                         userentityobj.firstname,
                         userentityobj.lastname,
                         userentityobj.usercommunication.mobileno,
                         userentityobj.usercommunication.emailid,
                         userentityobj.userlogin.username,
                         userentityobj.userlogin.password);

                    return (Convert.ToInt32(status.Value) == 1) ? true : false;
                });

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion


    }
}
