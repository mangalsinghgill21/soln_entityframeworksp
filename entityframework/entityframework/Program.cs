﻿using entityframework.Entity;
using entityframework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace entityframework
{
    class Program
    {
        static void Main(string[] args)
        {

            Task.Run(async () =>
            {

                userrepository userobj = new userrepository();

                #region Insert 

                Boolean flag = await userobj.InsertAsync(new user()
                {
                    firstname = "mangal singh",
                    lastname = "gill",
                    userlogin = new userlogin()
                        {
                            username = "sunny singh",
                            password = "boby"
                        },
                    usercommunication = new usercommunication()
                    {
                        emailid = "mangalsinghgill21@gmail.com",
                        mobileno = "8425052675"
                    }

                });


                #endregion

                #region Update Section
                Boolean flag = await userobj.UpdateAsync(new user()
                {
                    userid = 1,
                    firstname = "amanpreet",
                    lastname = "gill",
                    userlogin = new userlogin()
                        {
                            username = "boby",
                            password = "sunny"
                        },
                    usercommunication = new usercommunication()
                    {
                        emailid = "amanpreetsg15@gmail.com",
                        mobileno = "9967096684"
                    }
                });
                #endregion

                #region Delete Section
                Boolean flag = await userobj.DeleteAsync(new user()
                {
                    userid = 3
                });
                #endregion

            }).Wait();

        }
    }
}
